# DAT 390 project

## Intro:
This is a project where it was looked at the use of a CNN-LSTM (Convolutional neural network with combined with a long, short-term memory network), LR (Linear regression) and RF (Random forest) to predict the energy consumption of the "REFIT: Electrical Load Measurements" data set, that can be found at https://www.refitsmarthomes.org/datasets/. This repo contains all the code+images that were used in this project. This README is here to give an overview over all the files used in this project, key info about the files and code used and important information to know if one is to use the the code/work with the data oneself.

## Overview of the project:
* **Code**: The code used in the project
    * **Accuracy_testing.ipynb:** A jupyter notebook with the code used for training the models on the data from the different houses, and then making them make predictions for the consumption of the same houses.
    * **CNN_LSTM.py:** The python code for the CNN-LSTM model that was used
    * **Comparing_accuracy.ipynb:** A jupyter notebook with the code used for comapring the $R^2$- and $RMSE$-score of the CNN-LSTM, LR and RF on the different data from the different houses
    * **do_pca.py:** The Python code that can be used to do PCA on the data.
    * **exploring_the_data.ipynb:** A jupyter notebook with the code used for exploring the data of the differen houses.
    * **fetching_and_fix_data.py:** Python code for most of the function used in all of the notebooks
    * **setting_up_data.ipynb:** A jupyter notebook with the code used for fixing the data so that it can be used in "Accuracy_testing.ipynb" and "Comparing_accuracy.ipynb".
* **Plots:** Folder with subfolder that contains all the plots that were used.
    * **2014-10-01:** Contains all the plots for each house where the electric consumption of each appliance has been plotted.
    * **2014-10-01 cum:** Contains all the plots for each house where the electric consumption of all the appliances has been plotted as one graph.

## Dependencies:
Early on in this project there were some issues with the tensorflow and keras dependencies so below is a list with all of the packages that were used and their specific versions:
|Package            |Version|
|-------------------|:-----:|
|H5py               | 2.10.0|
|Keras-gpu          |  2.3.1|
|Keras-preprocessing|  1.1.2|
|Matplotlib         |  3.4.3|
|Numpy              | 1.19.2|
|Pandas             |  1.3.4|
|Python             | 3.7.11|
|Scikit-learn       |  1.0.1|
|Tensorflow         |  2.1.0|

## Where to find the data
The data used for the houses can be found at: <br>
https://pureportal.strath.ac.uk/en/datasets/refit-electrical-load-measurements-cleaned

It is labeled as "CLEAN_REFIT_081116.7z". All the houses within this file needs to have their names renamed from "CLEAN_House1.csv" to "House_1.csv", where the number 1 goes from 1 to 21, excluding 14. <br>

The weather data can be found at: <br>
https://repository.lboro.ac.uk/articles/dataset/REFIT_Smart_Home_dataset/2070091

Where an outline on how to use the data can be found on: https://www.refitsmarthomes.org/wp-content/uploads/2017/06/Climate_data.html. One does not have to do more than to run the code in the notebook **setting_up_data.ipynb** though.

## Naming convention for the data:
The untreated house measurement data is named as "House_1.csv", where the number 1 goes from 1 to 21, excluding 14. When this data has been processed it will be renamed to "House_1_15Min.csv". <br>

Regarding the weather data then after it has been processed by **setting_up_data.ipynb** then it will return the files "Air temperature.csv", "Average horizontal solar irradiance.csv" and "Total horizontal solar irradiance.csv". 

## Need to know about the data:  
The CSV files needed to make the notebooks work are not in this github repo due to their size. They can be imported as described above. They have to be added to the same folder as the jupyter files are runned from. <br>

Furthermore to be able to run "Accuracy_testing.ipynb" and "Comparing_accuracy.ipynb" then "setting_up_data.ipynb" **HAS TO BE RUNNED BEFORE** these jupyter notebooks to run correctly. <br>

## Good to know about the data:
* The data is not continuous, there are several gaps in the data where there are no readings. 
* Some of the appliances are changed half-way through the experiment
* Not all the appliances have data (see house 7)
* The houses are numbered from 1-13 and then from 15-21, why this is is not clear from the documentation. 

## Future work:
* There are many more readings in the same data set as the weather data. This would be interesting to take a closer look at in the future.
* The random forest did not find the weather data or time data to be of great importance for prediction. It would therefore be interesting to see how much removing these parameters would decrease/improve the performance of the models. 
* One should also try to use other models than the three used here to see if there is any improvement in the predictive power

## Contact info:
Name: Filip Kristoff Ørn 
Email: filip.kristoff@gmail.com
