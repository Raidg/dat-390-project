# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 08:40:56 2021

@author: Filip
"""

#%%
import numpy as np
import matplotlib.pyplot as plt
import h5py

from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Dense, LSTM, Reshape, Flatten, TimeDistributed
from tensorflow.keras import Model

#%%

def build_model(input_img, kernelheight, n_filters, lstm_param, 
                dense1_neurons, dense2_neurons, dropout):
    """
    This function builds the CNN-LSTM model that is tested against the RF-
    regressor.    

    Parameters
    ----------
    input_img : tensorflow.keras.layer.Input
        A keras input layer where it is expected that the layer is 3D and that 
        the first up to the thirteenth column, as shown as "i" in 
        [:, :, i-th], is the aggregate of the electric consumption + the 
        measurement of the electricity consumption of the 9 individual 
        appliances + the temperature, average solar irradiance and total solar
        irradiance.
        
    kernelheight : int
        The height of the pooling/2d convolutional filers. This is set so that
        one can decide how many previous timesteps that will be considered when 
        the convolutional neural networks runs over the data. 
        
    n_filters : int
        the number ov convolutional filters that will be applied to the data in
        each layer (for this model there is two layers).
        
    lstm_param : int
        Amount of units within the lstm layer.
        
    dense1_neurons : int
        Amount of neurons within the first dense layer.
        
    dense2_neurons : int
        Amount of neuron within the second dense layer.
        
    dropout : float from 0 to 1
        The percentage of the datapoints that will be dropped from one layer 
        to the next.

    Returns
    -------
    A tensorflow keras deep learning model

    """
    input_to_cnn = input_img[:, :, :13] # Extracts the relevant columns
    
    conv_layer1 = (Conv2D(
        filters=n_filters,
        kernel_size=(kernelheight, 1),
        strides=(1, 1), 
        padding='same', 
        activation='relu'
        ))(input_to_cnn)

    pool1 = (MaxPooling2D(
        (kernelheight, 1), 
        strides=(kernelheight,1), 
        padding='same'
        ))(conv_layer1)
    
    drop1 = Dropout(dropout)(pool1)
        
    conv_layer2 = (Conv2D(
        filters=n_filters, 
        kernel_size=(kernelheight, 1), 
        strides=(1, 1), 
        padding='same',
        activation='relu'))(drop1)
    
    pool2 = (MaxPooling2D(
        (kernelheight, 1), 
        strides=(kernelheight,1), 
        padding='same'))(conv_layer2)
    
    drop2 = Dropout(dropout)(pool2) 
    
    # This distributes the LSTM to all of the input
    lstm = TimeDistributed(LSTM(lstm_param, activation='tanh'))(drop2) 
    d1   = Dense(dense1_neurons, activation='relu')(lstm)
    d2   = Dense(dense2_neurons, activation='relu')(d1)
    d2   = Flatten()(d2)

    output = Dense(1)(d2)
    
    model = Model(inputs=[input_img], outputs=[output])
    return model