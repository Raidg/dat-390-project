# -*- coding: utf-8 -*-
"""
Created on Sat Dec 11 03:38:30 2021

@author: Filip
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler as SS

import datetime

from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error as mse

#%%


def get_cum_plots_from_date(date):
    """
    Function used to make the plots with the cumulative sum for each appliance

    Parameters
    ----------
    date : str
        A date on the form "Year-Month-Day".

    Returns
    -------
    None.

    """
    list_of_houses = [f'House_{i}.csv' for i in range(1, 22) if i != 14]
    
    list_of_houses_one_week = []
    
    # extracts all the data corresponding to one particular date:
    
    for house in list_of_houses:
        df = pd.read_csv(house)
        
        temp = df['Time'].str.extract('(^\d+\S\d+\S\d\d)')
        list_of_houses_one_week.append(df[df['Time'].str.contains(date)])
        del(df)
    
    del(temp)
   
    house_name = [hous.split('.')[0] for hous in list_of_houses]
    
    # Makes the plot for each house:    
    i = 0
    for house_data in list_of_houses_one_week:
        plt.figure()
        plt.plot(pd.to_datetime(house_data['Time']), house_data[house_data.columns[2]])
        cum_sum =  house_data[house_data.columns[3]]
        for appliance in house_data.columns[4:]:
            cum_sum += house_data[appliance]
        plt.plot(pd.to_datetime(house_data['Time']), cum_sum)
        time   = pd.to_datetime([f'{date} {j}:00' for j in range(24)])
        labels = [f'{j}:00' for j in range(24)]
        plt.tick_params(axis='x', labelsize=12)
        plt.tick_params(axis='y', labelsize=12)
        plt.xticks(time, labels)
        plt.xlabel('Time of measurement', fontsize = 20)
        plt.ylabel('Watt', fontsize = 20)
        plt.title(f'Aggregated power use of - {house_name[i]}', fontsize = 22)
        plt.legend(['Aggregate', 'Cumulative sum of appliances'], fontsize = 16)
        i += 1

#%%

def get_acc_plots_from_date(date):
    """
    Function used to make the plots with the cumulative sum for each appliance

    Parameters
    ----------
    date : str
        A date on the form "Year-Month-Day".

    Returns
    -------
    None.

    """
    list_of_houses = [f'House_{i}.csv' for i in range(1, 22) if i != 14]
    
    list_of_houses_one_week = []
    
    # extracts all the data corresponding to one particular date:
    
    for house in list_of_houses:
        df = pd.read_csv(house)
        
        temp = df['Time'].str.extract('(^\d+\S\d+\S\d\d)')
        list_of_houses_one_week.append(df[df['Time'].str.contains(date)])
        del(df)
    
    del(temp)
    
    # This list is used to map the measured energy consumption of the
    # appliances to the correct applaince
    house_appliances = [
    [
     'Aggregate', 'Fridge', 'Chest Freezer', 'Upright Freezer', 'Tumble Dryer', 
     'Washing Machine', 'Dishwasher', 'Computer Site', 'Television Site', 
     'Electric Heater'
     ],
    [
     'Aggregate', 'Fridge-Freezer', 'Washing Machine', 'Dishwasher', 
     'Television', 'Microwave', 'Toaster', 'Hi-Fi', 'Kettle', 
     'Oven Extractor Fan'
     ],
    [
     'Aggregate', 'Toaster', 'Fridge-Freezer', 'Freezer', 'Tumble Dryer',
     'Dishwasher', 'Washing Machine', 'Television', 'Microwave', 'Kettle'
     ],
    [
     'Aggregate', 'Fridge', 'Freezer', 'Fridge-Freezer', 'Washing Machine (1)',
     'Washing Machine (2)', 'Computer Site', 'Television Site', 'Microwave',
     'Kettle'
     ],
    [
     'Aggregate', 'Fridge-Freezer', 'Tumble Dryer', 'Washing Machine', 
     'Dishwasher', 'Computer Site', 'Television Site', 'Combination Microwave',
     'Kettle', 'Toaster'
     ],
    [
     'Aggregate', 'Freezer', 'Washing Machine', 'Dishwasher', 'MJY Computer', 
     'Television Site', 'Microwave', 'Kettle', 'Toaster', 'PGM Computer'
     ],
    [
     'Aggregate', 'Fridge', 'Freezer (Garage)', 'Freezer', 'Tumble Dryer',
     'Washing Machine', 'Dishwasher', 'Television Site', 'Toaster', 'Kettle'
     ],
    [
     'Aggregate', 'Fridge', 'Freezer', 'Dryer', 'Washing Machine', 'Toaster', 
     'Computer', 'Television Site', 'Microwave', 'Kettle'
     ],
    [
     'Aggregate', 'Fridge-Freezer', 'Washer Dryer', 'Washing Machine', 
     'Dishwasher', 'Television Site', 'Microwave', 'Kettle', 'Hi-Fi', 
     'Electric Heater'
     ],
    [
     'Aggregate', 'Fridge', 'Freezer', 'Chest Freezer', 'Fridge-Freezer',
     'Washing Machine', 'Dishwasher', 'Television Site', 'Microwave', 
     'Kenwood KMix'
     ],
    [
     'Aggregate', 'Fridge', 'Fridge-Freezer', 'Washing Machine', 'Dishwasher',
     'Computer Site', 'Microwave', 'Kettle', 'Router', 'Hi-Fi'
     ],
    [
     'Aggregate', 'Fridge-Freezer', 'Television Site(Lounge)', 'Microwave', 
     'Kettle', 'Toaster', 'Television Site(Bedroom)', 'Not Used', 'Not Used',
     'Not Used'
     ],
    [
     'Aggregate', 'Television Site', 'Unknown', 'Washing Machine', 
     'Dishwasher', 'Tumble Dryer', 'Television Site', 'Computer Site', 
     'Microwave', 'Kettle'
     ],
    [
     'Aggregate', 'Fridge-Freezer', 'Tumble Dryer', 'Washing Machine', 
     'Dishwasher', 'Computer Site', 'Television Site', 'Microwave', 
     'Kettle', 'Toaster'
     ],
    [
     'Aggregate', 'Fridge-Freezer (1)', 'Fridge-Freezer (2)', 
     'Electric Heater (1)', 'Electric Heater (2)', 'Washing Machine', 
     'Dishwasher', 'Computer Site', 'Television Site', 
     'Dehumidifier/Heater'
     ],
    [
     'Aggregate', 'Freezer (Garage)', 'Fridge-Freezer', 
     'Tumble Dryer (Garage)', 'Washing Machine', 'Computer Site', 
     'Television Site', 'Microwave', 'Kettle & toaster', 'Plug Site (Bedroom)'
     ],
    [
     'Aggregate', 'Fridge(garage)', 'Freezer(garage)', 'Fridge-Freezer', 
     'Washer Dryer(garage)', 'Washing Machine', 'Dishwasher', 
     'Desktop Computer', 'Television Site', 'Microwave'
     ],
    [
     'Aggregate', 'Fridge & Freezer', 'Washing Machine', 'Television Site', 
     'Microwave', 'Kettle', 'Toaster', 'Bread-maker', 'Lamp (80Watts)', 
     'Hi-Fi'
     ],
    [
     'Aggregate', 'Fridge', 'Freezer', 'Tumble Dryer', 'Washing Machine', 
     'Dishwasher', 'Computer Site', 'Television Site', 'Microwave', 'Kettle'
     ],
    [
     'Aggregate', 'Fridge-Freezer', 'Tumble Dryer', 'Washing Machine', 
     'Dishwasher', 'Food Mixer', 'Television', 'Kettle/Toaster', 
     'Vivarium', 'Pond Pump'
     ]
    ]
    
    # Some appliances changed during the experiment and so their names will be
    # changed depending upon the date.
    if pd.to_datetime(date) > pd.to_datetime('2014-11-21'):
        house_appliances[4][2] += ' & dehumidifier'
        
    if pd.to_datetime(date) > pd.to_datetime('2014-06-17'):
        house_appliances[9][1] = 'Blender'
        
    if pd.to_datetime(date) > pd.to_datetime('2014-06-25'):
        house_appliances[9][2] = 'Toaster'
        
    if pd.to_datetime(date) > pd.to_datetime('2014-06-25'):
        house_appliances[9][2] = 'Toaster'
        
    if pd.to_datetime(date) > pd.to_datetime('2013-11-24'):
        house_appliances[6][3] = 'Unknown'
        
        
    house_name = [hous.split('.')[0] for hous in list_of_houses]
    

    # Makes the plot for each house:    
    i = 0
    for house_data in list_of_houses_one_week:
        plt.figure()
        plt.plot(pd.to_datetime(house_data['Time']), house_data[house_data.columns[2]])
        for appliance in house_data.columns[3:]:
            plt.plot(pd.to_datetime(house_data['Time']), house_data[appliance])
        time   = pd.to_datetime([f'{date} {j}:00' for j in range(24)])
        labels = [f'{j}:00' for j in range(24)]
        plt.tick_params(axis='x', labelsize=12)
        plt.tick_params(axis='y', labelsize=12)
        plt.xticks(time, labels)
        plt.xlabel('Time of measurement', fontsize = 20)
        plt.ylabel('Watt', fontsize = 20)
        plt.title(f'Aggregated power use of - {house_name[i]}', fontsize = 22)
        plt.legend(house_appliances[i], fontsize = 16)
        i += 1

#%% The following code is used to import the external factors:

list_of_houses = [f'House_{i}_15Min.csv' for i in range(1, 22) if i != 14]

#%% 

def get_data(house):
    """
    This function fetches the data from a given house and
    returns the test and train set of that dataset. The 
    test set is selected as the first two weeks of October of
    2014 and March of 2015.

    Parameters
    ----------
    house : str
        The name of the house that the data is to be fetched 
        from.

    Returns
    -------
    list_of_split_time : Pandas DataFrame
        The training dataset
    list
        A list containing the test datasets.

    """
    year1  = 2014
    month1 = 10
    year2  = 2015
    month2 = 3
    
    X = pd.read_csv(house)
    
    # Extracts the times that is to be looked into:
    X['Year']      = pd.DatetimeIndex(X['Time']).year
    X['Month']     = pd.DatetimeIndex(X['Time']).month
    X['DayOfWeek'] = pd.DatetimeIndex(X['Time']).day_of_week
    X['Day']       = pd.DatetimeIndex(X['Time']).day
    X['Hour']      = pd.DatetimeIndex(X['Time']).hour
    X['Minutes']   = pd.DatetimeIndex(X['Time']).minute
    
    temp_date      = pd.to_datetime(X['Time'])
    X['Diff']      = temp_date.diff() # gets the time between one row and the 
                                      # previous
    
    # Extracts the data for October of 2014 which is to be used for testing.
    X_test1 = X[(
                    ((X['Day'] < 15) & (X['Month'] == month1)) | 
                    ((X['Day'] == 31) & (X['Month'] == (month1 - 1)))
                 ) & 
                (X['Year'] == year1)
                ]
        
    X = X[((X['Day'] > 7) | (X['Month'] != month1) & (X['Year'] == year1)) | (X['Year'] != year1)]
    
    # Extracts the data for March of 2015 which is to be used for testing.
    X_test2 = X[(
                     ((X['Day'] < 15) & (X['Month'] == month2)) | 
                     ((X['Day'] == 28) & (X['Month'] == (month2 - 1)))
                 ) & 
                (X['Year'] == year2)
                ]
        
    X = X[((X['Day'] > 7) | (X['Month'] != month2) & (X['Year'] == year2)) | (X['Year'] != year2)]
    
    # Finds the index where it is the best to split the dataframe into 
    # separate dataframes. This is done by finding where the difference 
    # between one dataframe and the next is more than 15 min, thus implying
    # a gap in the data. 
    points_to_split = X[X['Diff'].gt(datetime.timedelta(minutes = 15))].index
    
    list_of_split_time = []
    
    leng = len(points_to_split)
    
    # goes through the dataset and makes it into one long list with training 
    # sets
    for indx, element in enumerate(points_to_split):
        if indx == 0:
            list_of_split_time.append(X.iloc[:element]) # Extracts the first training set
        elif indx == (leng - 1):
            list_of_split_time.append(X.iloc[element:]) # Extracts the last training set
        else:
            list_of_split_time.append(X.iloc[points_to_split[indx - 1]:element]) # Fetches the training sets in between 
            
            
    return list_of_split_time, [X_test1, X_test2]

#%%

def scale_and_encode(X, scaler, columns_to_scale):
    """
    Transforms the hours, month and Day of the week into
    corresponding consine and sine values 

    Parameters
    ----------
    X : Pandas Dataframe
        A pandas dataframe with the values which shall be scaled
        and with an Hour, Month, DayOfWeek columns to transform.
    scaler : sklearn StandardScaler
        Instance of sklearn StandardScaler that has already been
        fitted with a training set.
    columns_to_scale : List of strings
        A list with names of the columns that are to be scaled.

    Returns
    -------
    Numpy array
        A numpy array with the a scaled X, with new columns for
        the sine and cosine transformation as explained above.
        

    """
    X[columns_to_scale] = scaler.transform(X[columns_to_scale].values)
    X['Hour sine'] = X.apply(lambda row: np.sin(2*np.pi*row['Hour']/24), axis=1)
    X['Hour cos'] = X.apply(lambda row: np.cos(2*np.pi*row['Hour']/24), axis=1)
    
    X['Month sine'] = X.apply(lambda row: np.sin(2*np.pi*row['Month']/12), axis=1)
    X['Month cos'] = X.apply(lambda row: np.cos(2*np.pi*row['Month']/12), axis=1)
    
    X['DayOfWeek sine'] = X.apply(lambda row: np.sin(2*np.pi*row['DayOfWeek']/7), axis=1)
    X['DayOfWeek cos'] = X.apply(lambda row: np.cos(2*np.pi*row['DayOfWeek']/7), axis=1)
    
    X = X.drop(columns=['Hour','Month', 'DayOfWeek'])
    
    return X.values

#%%

def make_flat_sets(X, X_test, k):
    """
    A function which create a 1D set of training and
    test sets

    Parameters
    ----------
    X : Pandas Dataframe
        Contains the list of subsamples of training data that
        within itself is continous. see function "get_data" 
        for more information on this. This list will be made 
        into a list of equal sized training samples
    X_test : Pandas Dataframe
        Contains the list of subsamples of test data that
        within itself is continous. see function "get_data" 
        for more information on this. This list will be made 
        into a list of equal sized training samples
    k : int
        The size of the sliding window.

    Returns
    -------
    X_train : Numpy array
        A numpy array with the training samples
    Y_train : Numpy array
        A numpy array with the true values of the 
        training samples.
    X_test : Numpy array
        A numpy array with the test samples
    Y_test : Numpy array
        A numpy array with the true values of the 
        test samples.

    """
    Y_train = []
    tot_train_set = []    
    scaler = SS(copy=False)
    
    columns_to_drop = ['Time', 'Year', 'Minutes', 'Issues', 'Day', 'Diff']
    
    columns_to_scale = ['Aggregate', 'Appliance1', 'Appliance2', 
                        'Appliance3', 'Appliance4', 'Appliance5', 
                        'Appliance6', 'Appliance7', 'Appliance8',
                        'Appliance9', 'Air temperature',
                        'Average horizontal solar irradiance',
                        'Total horizontal solar irradiation']
    
    # Fits the standard scaler
    for sublist in X:
        if sublist.shape[0] > k:
            sublist = sublist.drop(columns=columns_to_drop)
            scaler.partial_fit(sublist[columns_to_scale].values)
    
    # Scales and transforms the training data and makes it into one
    # long list of training samples:
    for sublist in X:
        if sublist.shape[0] > k:
            sublist = sublist.drop(columns=columns_to_drop)
            
            x_pwr = scale_and_encode(sublist, scaler, columns_to_scale)
            
            # selects the true values for the train data (aka the aggregate)
            Y_train.extend(x_pwr[k:, 0])
            
            # selects the training values
            for i in range(x_pwr.shape[0]-k):
                tot_train_set.append(x_pwr[i:(i+k)].flatten())
    
    X_train = tot_train_set

    Y_test = []
    
    tot_test_set = []

    # Scales and transforms the test data and makes it into one
    # long list of training samples:
    for sublist in X_test:
        if sublist.shape[0] > k:
            sublist = sublist.drop(columns=columns_to_drop)
            
            x_pwr = scale_and_encode(sublist, scaler, columns_to_scale)
            
            # selects the true values for the test data (aka the aggregate)
            Y_test.extend(x_pwr[95:, 0])
            
            # selects the data that will be tested 
            for i in range(x_pwr.shape[0]-95):
                tot_test_set.append(x_pwr[(95 - k + i):(i+95)].flatten())
            
    X_test = tot_test_set

    
    return np.array(X_train), np.array(Y_train), np.array(X_test), np.array(Y_test)

def make_3d_set(X, X_test, k, split):
    """
    A function which create a 3D set of training, test
    and validation sets

    Parameters
    ----------
    X : Pandas Dataframe
        Contains the list of subsamples of training data that
        within itself is continous. see function "get_data" 
        for more information on this. This list will be made 
        into a list of equal sized training samples
    X_test : Pandas Dataframe
        Contains the list of subsamples of test data that
        within itself is continous. see function "get_data" 
        for more information on this. This list will be made 
        into a list of equal sized training samples
    k : int
        The size of the sliding window.
    split : float
        A number in between 0 and 1 that selects how much of
        the training data that will be put off as validation 
        data.

    Returns
    -------
    X_train : Numpy array
        A numpy array with the training samples
    Y_train : Numpy array
        A numpy array with the true values of the 
        training samples.
    X_test : Numpy array
        A numpy array with the test samples
    Y_test : Numpy array
        A numpy array with the true values of the 
        test samples.
    X_valid : Numpy array
        A numpy array with the validation samples
    Y_valid : Numpy array
        A numpy array with the true values of the 
        validation samples.


    """
    Y_train = []
    tot_train_set = []    
    scaler = SS(copy=False)
    
    columns_to_drop = ['Time', 'Year', 'Minutes', 'Issues', 'Day', 'Diff']
    
    columns_to_scale = ['Aggregate', 'Appliance1', 'Appliance2', 
                        'Appliance3', 'Appliance4', 'Appliance5', 
                        'Appliance6', 'Appliance7', 'Appliance8',
                        'Appliance9', 'Air temperature',
                        'Average horizontal solar irradiance',
                        'Total horizontal solar irradiation']
    
    # Fits the standard scaler
    for sublist in X:
        if sublist.shape[0] > k:
            sublist = sublist.drop(columns=columns_to_drop)
            scaler.partial_fit(sublist[columns_to_scale].values)
    
    
    # Scales and transforms the training data and makes it into one
    # long list of training samples:    
    for sublist in X:
        if sublist.shape[0] > k:
            sublist = sublist.drop(columns=columns_to_drop)
            
            x_pwr = scale_and_encode(sublist, scaler, columns_to_scale)
            
            # selects the true values for the train data (aka the aggregate)
            Y_train.extend(x_pwr[k:, 0])
            
            # selects the training values
            for i in range(x_pwr.shape[0]-k):
                tot_train_set.append(x_pwr[i:(i+k)].reshape(k, x_pwr.shape[1], 1)) # the data is reshaped so that it is 3D for the model
    
    leng = len(tot_train_set)
    
    perm = np.random.permutation(leng)
    
    tot_train_set = np.array(tot_train_set)
    Y_train = np.array(Y_train)
    
    # Makes a validation and training set
    X_valid = tot_train_set[perm[int(leng*(1 - split) + 1):].astype(int)]
    Y_valid = Y_train[perm[int(leng*(1 - split) + 1):].astype(int)]
    
    X_train = tot_train_set[perm[:int(leng*(1 - split))].astype(int)]
    Y_train = Y_train[perm[:int(leng*(1 - split))].astype(int)]
    
    Y_test = []
    
    tot_test_set = []

    # Scales and transforms the test data and makes it into one
    # long list of training samples:
    for sublist in X_test:
        if sublist.shape[0] > k:
            sublist = sublist.drop(columns=columns_to_drop)
            
            x_pwr = scale_and_encode(sublist, scaler, columns_to_scale)
            
            # selects the true values for the test data (aka the aggregate)            
            Y_test.extend(x_pwr[95:, 0])
            
            # selects the data that will be tested             
            for i in range(x_pwr.shape[0]-95):
                tot_test_set.append(x_pwr[(95 - k + i):(i+95)].reshape(k, x_pwr.shape[1], 1))
            
    X_test = tot_test_set

    return np.array(X_train), np.array(Y_train), np.array(X_test), np.array(Y_test), np.array(X_valid), np.array(Y_valid)

#%%

def find_best_params(X_train, Y_train, X_test, Y_test, algo, params, name, house_nr):
    """
    Function that does a gridsearch to find the best param.
    Then those params are used to make a prediction, where the predictions are stored
    as a csv file.

    Parameters
    ----------
    X_train : Numpy array
        A numpy array with the training samples
    Y_train : Numpy array
        A numpy array with the true values of the 
        training samples.
    X_test : Numpy array
        A numpy array with the test samples
    Y_test : Numpy array
        A numpy array with the true values of the 
        test samples.
    algo : instance of machine learning model
        Machine learning model that can be trained.
    params : dict
        A dictionary for the parameters to be tested.
        on the form {param1:['1', '2.5', etc.], param2:[...],...}
    name : str
        Name of the algorithm to be trained.
    house_nr : int
        house number.

    Returns
    -------
    None.

    """
    clf = GridSearchCV(algo(), params, cv=10, n_jobs=3)
    clf.fit(X_train, Y_train)
             
    algo_pred = clf.predict(X_test) # Predict
    
    algo_pred = pd.DataFrame(algo_pred)
    algo_pred = algo_pred.rename(columns={algo_pred.columns[0]:clf.best_params_})
    pd.DataFrame(algo_pred).to_csv(f'{name}_House{house_nr}.csv') # stores prediction
    rms = mse(Y_test, algo_pred, squared=False) # Calculates the RMS
    print(f"Using {name} gave the RMSE of :", rms)

def fit_and_pred(X_train, Y_train, X_test, Y_test, algo, name, house_nr):
    """
    Function that uses a machinelearning model
    with already preset parameters to train and
    predict on new data. then the result is stores
    as a csv file.

    Parameters
    ----------
    X_train : Numpy array
        A numpy array with the training samples
    Y_train : Numpy array
        A numpy array with the true values of the 
        training samples.
    X_test : Numpy array
        A numpy array with the test samples
    Y_test : Numpy array
        A numpy array with the true values of the 
        test samples.
    algo : instance of machine learning model
        Machine learning model that can be trained.
    name : str
        Name of the algorithm to be trained.
    house_nr : int
        house number.

    Returns
    -------
    None.

    """
    algo.fit(X_train, Y_train)
    
    algo_pred = algo.predict(X_test)
    
    algo_pred = pd.DataFrame(algo_pred)
    pd.DataFrame(algo_pred).to_csv(f'{name}_House{house_nr}.csv') #stores prediction
    rms = mse(Y_test, algo_pred, squared=False) # Calculates the RMS
    print(f"Using {name} gave the RMSE of :", rms)
