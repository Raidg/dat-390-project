# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 12:31:32 2021

@author: Filip
"""


import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler as SS
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt

#%%

list_of_houses_one_week = []

list_of_houses = [f'House_{i}_r15min.csv' for i in range(1, 22) if i != 14]

#%%

def imp_and_scale(name):
    df = pd.read_csv(f'{name}.csv', index_col=1, names=['Id', 'Time', f'{name}'])
    df.index = pd.to_datetime(df.index)
    df.index = df.index.tz_convert(None)
    df[df.columns[-1]] = SS(copy=False).fit_transform(df[df.columns[-1]].values.reshape(-1, 1))
    return df.drop(columns=['Id'])

#%%

external_affectors = ['Air temperature', 'Average horizontal solar irradiance',
                      'Total horizontal solar irradiation']

affectors = imp_and_scale(external_affectors[0])

#%%

for add_aff in external_affectors[1:]:
    aff = imp_and_scale(add_aff)
    affectors = pd.merge(affectors, aff, left_index=True, right_index=True)

#%%

pca_list = []

list_of_houses = [f'House_{i}_r15min.csv' for i in range(1, 22) if i != 14]

for house in list_of_houses:
    df = pd.read_csv(house, index_col=0)
    df = df[df.columns[1:-1]]
    df.index = pd.to_datetime(df.index)
    df[df.columns] = SS(copy=False).fit_transform(df.values)
    
    df = pd.merge(df, affectors, left_index=True, right_index=True)
    
    improved_file = house.split('.')[0] + '_scaled.csv'
    df.to_csv(improved_file)
    
    pca_list.append(PCA().fit(df))
    
#%%

for count, pca in enumerate(pca_list):
    explnd_rtio = pca.explained_variance_ratio_
    plt.figure()
    plt.plot(range(1, len(explnd_rtio)+1), explnd_rtio)
    plt.plot(range(1, len(explnd_rtio)+1), [sum(explnd_rtio[:i]) for i in range(len(explnd_rtio))])
    plt.title(f'PCA components for house {count+1}')
    plt.xlabel('Number of components')
    plt.ylabel('percentage of explained variance')
    
